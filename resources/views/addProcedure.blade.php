@extends('layouts.app')

@section('content')
<div class="container">
            <!-- Hieronder staat de upload file systeem waar de procedure toegevoegd kan worden. -->
            <div class="addprocedurefield">
                <form action="store" method="post" enctype="multipart/form-data">

                    <!-- De procedure heeft een Code die ingevoerd moet worden -->
                    <label for="name">Code</label>
                    <input type="text" name="code"></br>

                    <!-- De procedute heeft ook een naam die ingevoerd kan worden -->
                    <label for="name">Name</label>
                    <input type="text" name="name"></br>


                    <label for="image"></label>
                    <input type="file" name="image"></br>

                    <!--
                    | De csrf_token is methode in programmeren.
                    | Het biedt eenvoudig bescherming tege Cross Site Request Vervalsingen.
                    | Dit type aanval vindt plaats wanneer  een kwaadaardige website een link,
                    | een vorm knop of een JavaScript die bedoeld is om wat actie op uw website uit te voeren,
                    | met behulp van de geloofsbrieven van een ingelogde gebruiker die de kwaadaardige site bezoekt in hun browser bevat.-->
                    <input type="hidden" name="_token" value="{{csrf_token()}}">

                    <!-- Knop Addprocedure -->
                    <button type="submit" class="btn btn-primary">
                      Add Procedure
                    </button>
                 </form>
              </div>
        </div>
     <!-- Dit zijn de rijen, cellen en kollommen waar procedures op staan -->
  </div>
@endsection
